package com.example.quanlycuahangsach.fragment;


import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.quanlycuahangsach.DanhSachTaiKhoanActivity;
import com.example.quanlycuahangsach.R;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class TaiKhoanFragment extends Fragment {
    Button btnDanhSachTaiKhoan,btnDoiMatKhau;

    public TaiKhoanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tai_khoan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponent();
        danhSachTaiKhoan();
        doiMatKhau();

    }

    public void initComponent(){
        btnDanhSachTaiKhoan=getView().findViewById(R.id.btnDanhSachTaiKhoan);
        btnDoiMatKhau=getView().findViewById(R.id.btnDoiMatKhau);
    }

    private void danhSachTaiKhoan(){
        btnDanhSachTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), DanhSachTaiKhoanActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doiMatKhau(){
        btnDoiMatKhau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

}
