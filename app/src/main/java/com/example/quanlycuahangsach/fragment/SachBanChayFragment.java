package com.example.quanlycuahangsach.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quanlycuahangsach.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SachBanChayFragment extends Fragment {


    public SachBanChayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sach_ban_chay, container, false);
    }

}
