package com.example.quanlycuahangsach;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlycuahangsach.dao.TaiKhoanDao;
import com.example.quanlycuahangsach.model.TaiKhoan;

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    TextView tvSignUp;
    TextInputEditText edtUsername, edtPassword;
    CheckBox chkNhoMatKhau;
    SharedPreferences storage;
    TaiKhoan taiKhoan;
    TaiKhoanDao taiKhoanDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //ẩn actionbar
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        initComponent();
        dangKy();
        checkLogin();
        saveInfo();
    }

    //ánh xạ
    public void initComponent() {
        btnLogin = findViewById(R.id.btnLogin);
        tvSignUp = findViewById(R.id.tvSignUp);
        edtUsername = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        chkNhoMatKhau = findViewById(R.id.chkNhoMatKhau);
    }

    //đăng ký
    public void dangKy() {
        //đăng ký
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ThemTaiKhoanActivity.class);
                startActivity(intent);
            }
        });
    }

    //đăng nhập
    public void checkLogin() {
        taiKhoanDao = new TaiKhoanDao(LoginActivity.this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                taiKhoan = new TaiKhoan();
                if (username.isEmpty() || password.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Tên đăng nhập và mật khẩu không được bỏ trống", Toast.LENGTH_SHORT).show();
                } else if (username.equals("kimtrong")&&password.equals("123456")){
                    Toast.makeText(getApplicationContext(), "Login thanh công", Toast.LENGTH_SHORT).show();
                    //kiểm tra checkbox đã check thì lưu dữ liệu đăng nhập
                    SharedPreferences.Editor editor = storage.edit();
                    if (chkNhoMatKhau.isChecked()) {
                        editor.putString("username", username);
                        editor.putString("password", password);
                    }
                    editor.putBoolean("save_infomation", true);
                    editor.commit();
                    //đăng nhập thành công chuyển sang màn hình chính
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                //đăng nhập không thành công
                else {
                    edtUsername.setText("");
                    edtPassword.setText("");
                    edtUsername.requestFocus();
                }
            }
        });
    }

    //lưu tài khoản và mật khẩu
    public void saveInfo() {
        //lưu tài khoản và mật khẩu
        storage = getSharedPreferences("myfile", MODE_PRIVATE);
        //nạp thông tin lên form từ SharedPreferances
        Boolean saveInfo = storage.getBoolean("save_infomation", false);
        if (saveInfo) {
            edtUsername.setText(storage.getString("username", ""));
            edtPassword.setText(storage.getString("password", ""));
            chkNhoMatKhau.setChecked(true);
        }
    }

}
