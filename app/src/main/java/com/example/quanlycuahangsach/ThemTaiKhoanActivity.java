package com.example.quanlycuahangsach;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.quanlycuahangsach.dao.TaiKhoanDao;
import com.example.quanlycuahangsach.model.TaiKhoan;

public class ThemTaiKhoanActivity extends AppCompatActivity {
    Button btnSignUp,btnCancel;
    TextInputEditText edtUserName,edtPassword,edtRePassword,edtFullName,edtPhone;
    TaiKhoanDao taiKhoanDao;
    TaiKhoan taiKhoan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_tai_khoan);
        setTitle("Đăng ký tài khoản");

        initComponent();
        signUp();
        cancelSignUp();
    }

    public void initComponent(){
        btnSignUp=findViewById(R.id.btnSignUp);
        btnCancel=findViewById(R.id.btnCancelSignUp);
        edtUserName=findViewById(R.id.edtUserName);
        edtPassword=findViewById(R.id.edtPassword);
        edtRePassword=findViewById(R.id.edtRePassword);
        edtFullName=findViewById(R.id.edtFullName);
        edtPhone=findViewById(R.id.edtPhone);
    }

    //xử lý đăng ký
    public void signUp(){
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //tạo đối tượng tài khoản và lấy dữ liệu nhập vào
                taiKhoan=new TaiKhoan();
                taiKhoan.setUsername(edtUserName.getText().toString());
                taiKhoan.setPassword(edtPassword.getText().toString());
                taiKhoan.setFullName(edtFullName.getText().toString());
                taiKhoan.setPhone(edtPhone.getText().toString());

                taiKhoanDao=new TaiKhoanDao(ThemTaiKhoanActivity.this);
                taiKhoanDao.insert(taiKhoan);
                finish();
            }
        });
    }

    //thoát đăng ký
    public void cancelSignUp(){
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
