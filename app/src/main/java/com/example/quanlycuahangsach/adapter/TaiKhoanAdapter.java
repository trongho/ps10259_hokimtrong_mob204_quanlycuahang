package com.example.quanlycuahangsach.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlycuahangsach.DanhSachTaiKhoanActivity;
import com.example.quanlycuahangsach.R;
import com.example.quanlycuahangsach.dao.TaiKhoanDao;
import com.example.quanlycuahangsach.model.TaiKhoan;

import java.util.List;

public class TaiKhoanAdapter extends BaseAdapter {
    List<TaiKhoan> taiKhoanList;
    public Context context;
    public LayoutInflater inflater;
    TaiKhoanDao taiKhoanDao;
    ViewHolder holder;

    public TaiKhoanAdapter(List<TaiKhoan> taiKhoanList, Context context) {
        super();
        this.taiKhoanList = taiKhoanList;
        this.context = context;
        this.inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        taiKhoanDao=new TaiKhoanDao(context);
    }

    @Override
    public int getCount() {
        return taiKhoanList.size();
    }

    @Override
    public Object getItem(int position) {
        return taiKhoanList.get(position);
    }

    public static class ViewHolder{
        ImageView ivTaiKhoanIcon;
        TextView tvName;
        TextView tvPhone;
        ImageView ivDelete;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final TaiKhoan taiKhoan=taiKhoanList.get(position);
        if(convertView==null)
        {
            holder=new ViewHolder();
            convertView=inflater.inflate(R.layout.one_cell_taikhoan,null);

            //ánh xạ
            holder.ivTaiKhoanIcon=(ImageView) convertView.findViewById(R.id.ivTaiKhoanIcon);
            holder.tvName=(TextView)convertView.findViewById(R.id.tvName);
            holder.tvPhone=(TextView)convertView.findViewById(R.id.tvPhone);
            holder.ivDelete=(ImageView) convertView.findViewById(R.id.ivDelete);

            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();

        //set data lên layout custom
        if (position % 3 ==0) {
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emone);
        }else if (position % 3 == 1){
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emtwo);
        }else {
            holder.ivTaiKhoanIcon.setImageResource(R.drawable.emthree);
        }
        holder.tvName.setText(taiKhoan.getFullName());
        holder.tvPhone.setText(taiKhoan.getPhone());

        //xóa tài khoản
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DanhSachTaiKhoanActivity)context).xoaTaiKhoan(taiKhoan);
            }
        });
        return convertView;
    }
}
