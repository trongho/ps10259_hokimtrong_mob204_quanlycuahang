package com.example.quanlycuahangsach;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.quanlycuahangsach.adapter.TaiKhoanAdapter;
import com.example.quanlycuahangsach.dao.TaiKhoanDao;
import com.example.quanlycuahangsach.model.TaiKhoan;

import java.util.ArrayList;

public class DanhSachTaiKhoanActivity extends AppCompatActivity {
    ListView lvDanhSachTaiKhoan;
    TaiKhoanAdapter taiKhoanAdapter;
    ArrayList<TaiKhoan> list;
    TaiKhoanDao taiKhoanDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danh_sach_tai_khoan);

        initComponent();
        showDanhSachTaiKhoan();
    }

    public void initComponent(){
        lvDanhSachTaiKhoan=findViewById(R.id.lvDanhSachTaiKhoan);
    }

    public void showDanhSachTaiKhoan(){
        list=new ArrayList<>();
        taiKhoanDao=new TaiKhoanDao(DanhSachTaiKhoanActivity.this);
        list=taiKhoanDao.getTaiKhoan();
        taiKhoanAdapter=new TaiKhoanAdapter(list,DanhSachTaiKhoanActivity.this);
        lvDanhSachTaiKhoan.setAdapter(taiKhoanAdapter);
    }

    public void xoaTaiKhoan(TaiKhoan taiKhoan){
        taiKhoanDao.delete(taiKhoan);
        capNhatLV();
    }

    public void capNhatLV(){
        taiKhoanAdapter.notifyDataSetChanged();
    }
}
