package com.example.quanlycuahangsach.dao;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.quanlycuahangsach.LoginActivity;
import com.example.quanlycuahangsach.MainActivity;
import com.example.quanlycuahangsach.model.TaiKhoan;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.support.constraint.Constraints.TAG;

public class TaiKhoanDao {
    private DatabaseReference mDatabase;
    private Context context;
    String taikhoanID;

    public TaiKhoanDao(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("TaiKhoan");
        this.context = context;
    }

    //lấy tất cả tài khoản
    public ArrayList<TaiKhoan> getTaiKhoan() {
        final ArrayList<TaiKhoan> list = new ArrayList<>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    TaiKhoan taiKhoan = dataSnapshot.getValue(TaiKhoan.class);
                    list.add(taiKhoan);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    //thêm tài khoản
    public void insert(TaiKhoan taiKhoan) {
        taikhoanID = mDatabase.push().getKey();
        mDatabase.child(taikhoanID).setValue(taiKhoan)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Thêm thành công", Toast.LENGTH_LONG).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Thêm thất bại", Toast.LENGTH_LONG).show();
                    }
                });
    }

    //sửa tài khoản
    public void update(final TaiKhoan taiKhoan) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("username").getValue(String.class).equalsIgnoreCase(taiKhoan.getUsername())) {
                        taikhoanID = data.getKey();
                        mDatabase.child(taikhoanID).setValue(taiKhoan)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Sửa thành công", Toast.LENGTH_LONG).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Sửa thất bại", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //xóa tài khoản
    public void delete(final TaiKhoan taiKhoan) {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("username").getValue(String.class).equalsIgnoreCase(taiKhoan.getUsername())) {
                        taikhoanID = data.getKey();
                        mDatabase.child(taikhoanID).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_LONG).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_LONG).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

